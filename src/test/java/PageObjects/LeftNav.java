package PageObjects;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by kolpa on 17.10.2016.
 */
public class LeftNav {
    public SelenideElement getActA() {
        return $("#navigationLeftTab_AT0");
    }

    public SelenideElement getInactA() {
        return $("#navigationLeftTab_AT1");
    }

    public SelenideElement getAct() {
        return $("#navigationLeftTab_T0");
    }

    public SelenideElement getInact() {
        return $("#navigationLeftTab_T1");
    }

    public SelenideElement getActivTab() {
        String str = getActA().getAttribute("class");
        if (str.indexOf("activeTab") != -1) {
            return getActA();
        } else
            return getInactA();
    }

    public SelenideElement getInactivTab() {
        String str = getInactA().getAttribute("class");
        if (str.indexOf("activeTab") != -1) {
            return getActA();

        } else
            return getInactA();

    }
}
