package PageObjects;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by kolpa on 17.10.2016.
 */
public class DashboardDoctor {

    public SelenideElement getBtnNewPatient() {
        return $("#btnNewPatient_CD");
    }

    public ElementsCollection getPatientList() {
        return $$(By.className("patientItem"));
    }

    public ElementsCollection getTabs() {
        return $("#PatientVisitsTabt_TC").findAll(By.tagName("span"));
    }

    public SelenideElement getTab(String title) {
        for (SelenideElement tab : getTabs()) {
            if (tab.getText().equals(title))
                return tab;
        }
        return null;
    }

    public boolean isActiveTab(String title){
        ElementsCollection tabs = $("#PatientVisitsTabt_TC").findAll(By.tagName("li"));
        for(int i =0;i < tabs.size();i++){
            if (tabs.get(i).getAttribute("class").indexOf("activeTab") != -1){
                if (getTabs().get(i).getText().equals(title))
                    return true;
                else
                    return false;
            }
        }
        return false;
    }

    public int getIndexListPatient(String name) {
        boolean have = false;
        int i = 0;
        for (; i < getPatientList().size(); i++) {
            String patinentName = getPatientList().get(i).find(By.className("dxeBase_ClinicalStudyTheme")).getText();
            if (name.equals(patinentName)) {
                have = true;
                break;
            }
        }
        if (have) {
            return i;
        } else
            return -1;
    }
}
