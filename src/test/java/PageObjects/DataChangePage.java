package PageObjects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by kolpa on 17.10.2016.
 */
public class DataChangePage {
    public SelenideElement getBtnSave() {
        return $("#NoteSave1_CD");
    }
    public SelenideElement getTextBlock() {
        return $(By.tagName("textarea"));
    }
}
