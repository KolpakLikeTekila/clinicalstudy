package PageObjects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by kolpa on 17.10.2016.
 */
public class DemographicsPage {
    public SelenideElement getBtnSave(){
        return $("#btnSave1_CD");
    }

    public SelenideElement getBtnEdit(){
        return $("#btnEditDemog_CD");
    }

    public SelenideElement getOtcher(){
        return $("#Other");
    }

    public void setOther(String txt){
        getOtcher().$(By.tagName("textarea")).val(txt);
    }
}
