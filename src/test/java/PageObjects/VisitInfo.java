package PageObjects;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by kolpa on 17.10.2016.
 */
public class VisitInfo {

    public SelenideElement getVisitDate() {
        return $("#VisitDate");
    }

    public SelenideElement getVisitTime() {
        return $("#VisitTime");
    }

    public SelenideElement getExpectedVisitDate() {
        return $("#ExpectedVisitDate");
    }
}