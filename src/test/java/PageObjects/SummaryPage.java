package PageObjects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

/**
 * Created by kolpa on 17.10.2016.
 */
public class SummaryPage {

    public void setPatientInitials(String txt) {
        $("#PatientInitials_I").val(txt);
    }

    public SelenideElement getPatientInitialsError() {
        return $("#PatientInitials_ETC");
    }

    public SelenideElement getBtnSave() {
        return $("#btnSave_CD");
    }

    public SelenideElement getBtnEdit() {
        return $("#btnEditPatient_CD");
    }

    public SelenideElement getEnrolled() {
        return $("#IsEnrolled_S_D");
    }

    public SelenideElement getActive() {
        return $("#IsActive_S_D");
    }

    public SelenideElement getEnrollDate() {
        return $("#EnrollDate_I");
    }

    public SelenideElement getRandomisationNumber() {
        return $("#RandomisationNumber_I");
    }

    public SelenideElement getRandomisationDate() {
        return $("#RandomisationDate_I");
    }

    public boolean isSelectCheckBox(SelenideElement box) throws Exception {
        String str = box.getAttribute("class");
        if (str.indexOf("Checked") != -1) {
            return true;
        } else if (str.indexOf("Unchecked") != -1) {
            return false;
        } else {
            throw new Exception("Не чекбокс");
        }
    }

    public String getPatientNumber() {
        return $$(By.className("editor-field")).get(1).find(By.tagName("span")).getText();
    }
}
