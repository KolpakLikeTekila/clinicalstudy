/**
 * Created by kolpa on 17.10.2016.
 */

import PageObjects.*;
import org.junit.*;

import static com.codeborne.selenide.Condition.disappears;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.clearBrowserCache;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.isIE;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class DashboardTest {


    @Before
    public void Login() {
        if (isIE()) {
            clearBrowserCache();
        }
        open("https://demos.devexpress.com/RWA/ClinicalStudy/");
        $("#uc_dmitchell").click();
        if (isIE()) {
            sleep(2000);
        }
        $("#Logondmitchell_CD").click();
        waitUntilPagesIsLoaded();
    }

    @AfterClass
    public static void ieRelax() {
        if (isIE()) {
            closeWebDriver();
            sleep(3000);
        }
    }

    private void waitUntilPagesIsLoaded() {
        if ($("#loadingIndicator").isDisplayed()) {
            $("#loadingIndicator").waitUntil(disappears, 20000);
        }

    }

    @Test
    public void rewriteOtherDemogTest() {
        DashboardDoctor pageMain = page(DashboardDoctor.class);
        int n = pageMain.getPatientList().size();
        pageMain.getPatientList().last().click();
        waitUntilPagesIsLoaded();

        if (!pageMain.isActiveTab("Baseline")){
            pageMain.getTab("Baseline").click();
        }
        waitUntilPagesIsLoaded();
        DemographicsPage page = page(DemographicsPage.class);
        page.getBtnEdit().click();
        String otherOld = page.getOtcher().getText();
        page.setOther("test123");
        page.getBtnSave().click();
        waitUntilPagesIsLoaded();

        DataChangePage pageChange = page(DataChangePage.class);
        String otherNew = "testRewrite";
        pageChange.getTextBlock().val(otherNew);
        pageChange.getBtnSave().click();
        waitUntilPagesIsLoaded();

        page.getBtnSave();
        waitUntilPagesIsLoaded();

        assertNotEquals(page.getOtcher().getText(), otherOld);
    }

    @Test
    public void createNewPatientTest() throws Exception {
        DashboardDoctor pageMain = page(DashboardDoctor.class);
        assertNotEquals(-1, pageMain.getIndexListPatient(createNewPatient()));
    }

    public String createNewPatient() throws Exception {
        DashboardDoctor pageMain = page(DashboardDoctor.class);

        pageMain.getBtnNewPatient().click();
        waitUntilPagesIsLoaded();

        SummaryPage pageCreatePatient = page(SummaryPage.class);
        if (!pageCreatePatient.isSelectCheckBox(pageCreatePatient.getActive())) {
            pageCreatePatient.getActive().click();
        }
        if (!pageCreatePatient.isSelectCheckBox(pageCreatePatient.getEnrolled())) {
            pageCreatePatient.getEnrolled().click();
        }
        pageCreatePatient.getEnrollDate().val("10/10/2010");
        pageCreatePatient.getRandomisationDate().val("10/10/2010");
        pageCreatePatient.getRandomisationNumber().val("12345");
        pageCreatePatient.setPatientInitials("AB");
        pageCreatePatient.getBtnSave().click();

        waitUntilPagesIsLoaded();

        return "Subj A" + pageCreatePatient.getPatientNumber();
    }

    @Test
    public void createPatientInitNumberTest() throws Exception {
        DashboardDoctor pageMain = page(DashboardDoctor.class);
        pageMain.getBtnNewPatient().click();
        waitUntilPagesIsLoaded();
        SummaryPage pageCreatePatient = page(SummaryPage.class);

        pageCreatePatient.setPatientInitials("123");
        pageCreatePatient.getBtnSave().click();
        waitUntilPagesIsLoaded();

        String patient = "Subj A" + pageCreatePatient.getPatientNumber();
        assertEquals(-1, pageMain.getIndexListPatient(patient));
    }

    @Test
    public void createPatientInitEmtyTest() throws Exception {
        DashboardDoctor pageMain = page(DashboardDoctor.class);
        pageMain.getBtnNewPatient().click();
        waitUntilPagesIsLoaded();
        SummaryPage pageCreatePatient = page(SummaryPage.class);

        pageCreatePatient.getRandomisationNumber().val("12345");
        pageCreatePatient.getBtnSave().click();
        waitUntilPagesIsLoaded();

        String patient = "Subj A" + pageCreatePatient.getPatientNumber();
        assertEquals(-1, pageMain.getIndexListPatient(patient));
        pageCreatePatient.getPatientInitialsError().shouldHave(text("Please enter Patient Initials"));
    }

    @Test
    public void dropPatientInactiveTest() throws Exception {
        DashboardDoctor pageMain = page(DashboardDoctor.class);
        LeftNav tab = page(LeftNav.class);
        if (!tab.getActivTab().equals(tab.getActA())) {
            tab.getAct().click();
        }
        if (pageMain.getPatientList().size() == 0) {
            createNewPatient();
        }
        pageMain.getPatientList().get(0).click();
        waitUntilPagesIsLoaded();
        if (!pageMain.isActiveTab("Summary")){
            pageMain.getTab("Summary").click();
        }
        waitUntilPagesIsLoaded();
        SummaryPage pageCreatePatient = page(SummaryPage.class);

        pageCreatePatient.getBtnEdit().click();
        pageCreatePatient.getActive().click();
        pageCreatePatient.getBtnSave().click();

        waitUntilPagesIsLoaded();
        pageMain = page(DashboardDoctor.class);
        String patientSubj = "Subj A" + pageCreatePatient.getPatientNumber();
        assertEquals(-1, pageMain.getIndexListPatient(patientSubj));

        tab.getInact().click();
        waitUntilPagesIsLoaded();
        sleep(500);
        pageMain = page(DashboardDoctor.class);

        assertNotEquals(-1, pageMain.getIndexListPatient(patientSubj));
    }


}




